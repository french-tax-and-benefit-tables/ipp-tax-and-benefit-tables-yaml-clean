Titre: 'Barèmes IPP : retraites'
Description: Ce document présente l'ensemble des barèmes des régimes de retraite obligatoires français. Les sources législatives
  (texte de loi, numéro du décret ou arrêté) ainsi que la date de publication au Journal Officiel (JO) sont systématiquement
  indiquées.
Sommaire:
  I. Secteur privé:
    reval-p: Revalorisation des pensions
    reval-s: Revalorisation des salaires portés aux comptes
    reval-s-49: Revalorisation des salaires (1949-1952)
    salval: Salaire validant un trimestre d'assurance
    aod-rg: Age d'ouverture des droits (dit "âge légal")
    aad-rg: Age d'annulation de la décote
    montant-mico: Montant du minimum contributif
    plafond-mico: Plafond du minimum contributif
    prorat-rg: Durée d'assurance maximale pour le coefficient de proratisation
    trimtp-rg: Durée d'assurance cible pour le taux plein
    sam: Nombre d'années prises en compte dans la calcul du SAM
    decote-rg: Decote RG
    surcote-rg: Surcote RG
    pt-arrco: Valeur du point ARRCO
    salref-arrco: Salaire de référence ARRCO
    coeff-mino-arrco: Coefficient de minoration ARRCO
    pt-agirc: Valeur du point AGIRC
    salref-agirc: Salaire de référence AGIRC
    coeff-mino-agirc: Coefficient de minoration AGIRC
    pt-unirs-arrco: Valeur du point ARRCO, UNIRS (1957-1998)
    salref-unirs: Salaire de référence ARRCO, UNIRS (1957-1998)
    pt-probtp-arrco: Valeur du point ARRCO, PRO-BTP (1960-1998)
    salref-probtp: Salaire de référence ARRCO, PRO-BTP (1961-1998)
    gmp: Garantie minimale de points (GMP) à l'Agirc
    coeff-temp: Coefficients de solidarité et majorant temporaires
  II. Secteur public:
    la-fp-s: Limite d'âge FP sédentaire
    la-fp-a: Limite d'âge  FP active
    aod-fp-s: Age légal de départ en retraite  FP sédentaire
    aod-fp-a: Age légal de départ en retraite  FP active
    aad-fp: Age d'annulation de la décote FP
    decote-fp: Decote FP
    surcote-fp: Surcote FP
    prorat-fp: Durée de service maximale pour le coefficient de proratisation FP
    trimtp-fp: Durée de service maximale cible pour le taux plein FP
    pt-ircantec: Valeur du point IRCANTEC
    salref-ircantec: Salaire de référence IRCANTEC
    rafp: Valeur du point RAFP
  III. Indépendants:
    ram: Nombre d'années prises en compte dans le calcul du RAM
    pt-organic: Valeur des points acquis avant 1973 - ORGANIC
    pt-cancava: Valeur des points acquis avant 1973 - CANCAVA
    pt-rci: 'Régime complémentaire des indépendants : valeur de service du point'
    salref-rci: 'Régime complémentaire des indépendants : valeur d''achat du point'
    pt-rc-art: 'Régime complémentaire des artisans : valeur de service du point (1980-2012)'
    salref-rc-art: 'Régime complémentaire des artisans : valeur d''achat du point (1979-2012)'
    pt-rc-com: 'Régime complémentaire des commerçants : valeur de service du point (2004-2012)'
    salref-rc-com: 'Régime complémentaire des commerçants : valeur d''achat du point (2005-2012)'
    rsi-vp: 'Régime social des indépendants : valeur du point retraite'
Notes: |-
  Citer cette source :
  Barèmes IPP: retraites, Institut des politiques publiques, janvier 2018

  Auteurs :
  Benjamin Belrhomari, Antoine Bozio, Sophie Cottet, Louise Paul-Delvaux et Simon Rabaté

  Contacts :
  antoine.bozio@ipp.eu
  simon.rabate@ipp.eu
Données initiales:
  Producteur: Institut des politiques publiques
  Format: XLS
  URL: http://www.ipp.eu/outils/baremes-ipp/
Convertisseur:
  URL: https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-converters
Données générées:
  Format: YAML
  URL: https://git.framasoft.org/french-tax-and-benefit-tables/ipp-tax-and-benefit-tables-yaml-raw
Licence: Licence ouverte <http://www.etalab.gouv.fr/licence-ouverte-open-licence>
